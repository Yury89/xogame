package com.kpi.XOGame.model;

import java.net.Socket;

/**
 * Created by Yuriy on 1/24/2016.
 */
public class User {
    private Point point;
    private Socket socket;

    public User(Point point, Socket socket)
    {
        this.point = point;
        this.socket = socket;
    }

    public Socket getSocket() {
        return socket;
    }

    public Point getPoint() {
        return point;
    }
}
