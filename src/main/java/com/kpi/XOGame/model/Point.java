package com.kpi.XOGame.model;

/**
 * Created by Yuriy on 1/24/2016.
 */
public enum Point {
    NULL,
    X,
    O
}
