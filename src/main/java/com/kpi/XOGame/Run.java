package com.kpi.XOGame;

import com.kpi.XOGame.controller.ModelController;
import com.kpi.XOGame.model.Point;
import com.kpi.XOGame.model.User;

/**
 * Created by Yuriy on 1/24/2016.
 */

public class Run {
    public static void main(String[] args) {
        ModelController modelController = new ModelController();
        User user1 = modelController.getUser1();
        User user2 = modelController.getUser2();

        while (modelController.isContinue() == Point.NULL) {
            modelController.addPoint(modelController.getStep() % 2 == 0 ? user1 : user2);

        }
        if(modelController.isContinue() == Point.X)
            System.out.println("X Won the game");
        else System.out.println("Y won the Game");

   }

}
