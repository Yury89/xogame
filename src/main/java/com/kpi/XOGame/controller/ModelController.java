package com.kpi.XOGame.controller;

import com.kpi.XOGame.model.Model;
import com.kpi.XOGame.model.Point;
import com.kpi.XOGame.model.User;
import com.kpi.XOGame.view.View;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

/**
 * Created by Yuriy on 1/24/2016.
 */
public class ModelController {
    public Model model = new Model();
    private View view = new View();
    private ServerSocket serverSocket;
    private Socket socket;
    private InputStream inputStream;
    private User user1;
    private User user2;
    private OutputStream outputStream;
    private  int step;

    public int getStep() {
        return step;
    }

    {
        step = 0;

        setPoint();

        setPoint();
    }

    public User getUser1() {
        return user1;
    }

    public User getUser2() {
        return user2;
    }

    public void sendTurn(String str, User user){


        try {
            int port;
            if(user.getPoint() == Point.X)
                port = 8992;
            else port = 8994;
            socket = new Socket("localhost", port);
            outputStream = socket.getOutputStream();
            outputStream.write(str.getBytes());
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }




    public void addPoint(User user) {
        String str1 = getPoint(user);

        sendTurn(str1,user);
        String[]str = str1.split(" ");

        int x = Integer.parseInt(str[0]);
        int y = Integer.parseInt(str[1]);
        step++;
        model.field[x][y] = user.getPoint();
        System.out.println(model);
    }

    public void setPoint(){
        try {
            serverSocket = new ServerSocket(8993);
            socket = serverSocket.accept();
            outputStream = socket.getOutputStream();
            if(user1 == null){
                user1 = new User(Point.X,socket);
                System.out.println("set user1");

                outputStream.write("X".getBytes());
                outputStream.flush();
            }
            else {
                user2 = new User(Point.O, socket);
                System.out.println("set user2");
                outputStream.write("O".getBytes());
                outputStream.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            close();
        }
    }
    public Point isContinue() {
        int winX = 0;
        int winO = 0;
        for (int i = 0; i < model.field.length; i++) {
            for (int j = 0; j < model.field[i].length; j++) {
                if (model.field[i][j] == Point.O)
                    winO++;
                if (model.field[i][j] == Point.X)
                    winX++;
            }
            if (winO == 3)
                return Point.O;
            if (winX == 3)
                return Point.X;
            winO = 0;
            winX = 0;

        }
        return Point.NULL;
    }


    private String getPoint(User user) {
        String result = "";
        try {
            serverSocket = new ServerSocket(8993);
            view.addPoint(user);
            socket = serverSocket.accept();
            inputStream = socket.getInputStream();
            do {
                result += (char) inputStream.read();
            }
                while (inputStream.available() > 0) ;

            return result;
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            close();
        }

        return result;
    }

    private void close() {
        try {

            if (serverSocket != null)
                serverSocket.close();
            if (socket != null)
                serverSocket.close();
            if(inputStream != null)
                inputStream.close();
            if(outputStream != null)
                outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
